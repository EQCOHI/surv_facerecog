# -*- coding: utf-8 -*-

from __future__ import print_function
import os
import cv2
from models import *
import torch
import numpy as np
import time
# from config import Config
from config.config import Config
from torch.nn import DataParallel
import matplotlib.pyplot as plt
from sklearn import metrics

def get_lfw_list(pair_list):
    with open(pair_list, 'r') as fd:
        pairs = fd.readlines()
    data_list = []
    for pair in pairs:
        splits = pair.split()

        if splits[0] not in data_list:
            data_list.append(splits[0])

        if splits[1] not in data_list:
            data_list.append(splits[1])
    return data_list


def load_image(img_path):
    image = cv2.imread(img_path, 0)
    # for LR 16x16 -> 128x128 by bicubic interpolation. if not the case use LR, remove next line
    # image = cv2.resize(image, dsize=(128,128), interpolation=cv2.INTER_CUBIC)
    if image is None:
        return None
    image = np.dstack((image, np.fliplr(image)))
    image = image.transpose((2, 0, 1))
    image = image[:, np.newaxis, :, :]
    image = image.astype(np.float32, copy=False)
    image -= 127.5
    image /= 127.5
    return image


def get_featurs(model, test_list, batch_size=10):
    images = None
    features = None
    cnt = 0
    for i, img_path in enumerate(test_list):
        image = load_image(img_path)
        if image is None:
            print('read {} error'.format(img_path))

        if images is None:
            images = image
        else:
            images = np.concatenate((images, image), axis=0)

        if images.shape[0] % batch_size == 0 or i == len(test_list) - 1:
            cnt += 1

            data = torch.from_numpy(images)
            data = data.to(torch.device("cuda"))
            output = model(data)
            output = output.data.cpu().numpy()

            fe_1 = output[::2]
            fe_2 = output[1::2]
            feature = np.hstack((fe_1, fe_2))
            # print(feature.shape)

            if features is None:
                features = feature
            else:
                features = np.vstack((features, feature))

            images = None

    return features, cnt


def load_model(model, model_path):
    model_dict = model.state_dict()
    pretrained_dict = torch.load(model_path)
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
    model_dict.update(pretrained_dict)
    model.load_state_dict(model_dict)


def get_feature_dict(test_list, features):
    fe_dict = {}
    for i, each in enumerate(test_list):
        # key = each.split('/')[1]
        fe_dict[each] = features[i]
    return fe_dict


def cosin_metric(x1, x2):
    return np.dot(x1, x2) / (np.linalg.norm(x1) * np.linalg.norm(x2))


def cal_accuracy(y_score, y_true):
    y_score = np.asarray(y_score)
    y_true = np.asarray(y_true)
    best_acc = 0
    best_th = 0
    for i in range(len(y_score)):
        th = y_score[i]
        y_test = (y_score >= th)
        acc = np.mean((y_test == y_true).astype(int))
        if acc > best_acc:
            best_acc = acc
            best_th = th

    return (best_acc, best_th)

def find_nearst(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def plot_roc(tpr, far, th):
    roc_auc = metrics.auc(far, tpr)
    print("AUC: ", roc_auc)
    plt.figure()
    plt.plot(far, tpr, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0,1],[0,1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC Curve')
    plt.legend(loc='lower right')
    plt.show()

def get_FRR(y_score, y_true, num_of_positive):
    th_list = np.arange(0, 4, 0.01)
    frr = []
    tpr = []
    for tmp_th in th_list:
        y_test = (y_score < tmp_th)[:num_of_positive]
        frr.append((list(y_test).count(1))/len(y_test))
        tpr.append((list(y_test).count(0))/len(y_test))
    frr = np.array(frr)
    return frr, tpr

def get_FAR(y_score, y_true, num_of_negative):
    th_list = np.arange(0, 4, 0.01)
    far = []
    threshold = []
    for tmp_th in th_list:
        y_test = (y_score >= tmp_th)[num_of_negative:]
        far.append((list(y_test).count(1))/len(y_test))
        threshold.append(tmp_th)

    far = np.array(far)
    return far, threshold

def get_EER(y_score, y_true, num_of_positive, num_of_negative):
    y_score = np.asarray(y_score)
    y_true = np.asarray(y_true)
    y_score += 1
    y_score = np.square(y_score)

    frr, tpr = get_FRR(y_score, y_true, num_of_positive)
    far, th = get_FAR(y_score, y_true, num_of_negative)

    diff = []
    for i in range(400):
        a = frr[i]
        b = far[i]
        diff.append(abs(b-a))
    idx = diff.index(min(diff))

    EER = far[idx]
    best_th = th[idx]
    print('EER={}, threshold={}'.format(EER, best_th))
    print("*****************************************************")
    fig, ax = plt.subplots()
    ax.plot(th, far, 'r--', label='FAR')
    ax.plot(th, frr, 'g--', label='FRR')
    plt.xlabel('Threshold')
    plt.plot(th[idx], EER, 'ro', label='EER')
    legend = ax.legend(loc='upper center', fontsize='small')
    legend.get_frame().set_facecolor('C0')
    plt.show()

    tar_far = [0.3, 0.1, 0.01, 0.001]
    for item in tar_far:
        print("tar@far={}:".format(item))
        idx = find_nearst(far, item)
        print(tpr[idx])

    # plot roc-curve
    plot_roc(tpr, far, th)

def test_performance(fe_dict, pair_list):
    with open(pair_list, 'r') as fd:
        pairs = fd.readlines()

    sims = []
    labels = []
    num_of_positive_pair = 0
    num_of_negative_pair = 0
    for pair in pairs:
        splits = pair.split()
        fe_1 = fe_dict[splits[0]]
        fe_2 = fe_dict[splits[1]]
        label = int(splits[2])
        if label == 1:
            num_of_positive_pair += 1
        else:
            num_of_negative_pair += 1

        sim = cosin_metric(fe_1, fe_2)
        # sim = np.sum(np.square(np.subtract(fe_1, fe_2)))
        sims.append(sim)
        labels.append(label)

    # normalize
    # sims = [(float(i)-min(sims))/(max(sims)-min(sims)) for i in sims]

    get_EER(sims, labels, num_of_positive_pair, num_of_negative_pair)
    acc, th = cal_accuracy(sims, labels)
    return acc, th


def lfw_test(model, img_paths, identity_list, compair_list, batch_size):
    s = time.time()
    features, cnt = get_featurs(model, img_paths, batch_size=batch_size)
    print('feature shape: ',features.shape)
    t = time.time() - s
    print('total time is {}, average time is {}'.format(t, t / cnt))
    fe_dict = get_feature_dict(identity_list, features)
    acc, th = test_performance(fe_dict, compair_list)
    print('lfw face verification accuracy: ', acc, 'threshold: ', th)
    return acc


if __name__ == '__main__':

    opt = Config()
    if opt.backbone == 'resnet18':
        model = resnet_face18(opt.use_se)
    elif opt.backbone == 'resnet34':
        model = resnet34()
    elif opt.backbone == 'resnet50':
        model = resnet50()

    model = DataParallel(model)
    # load_model(model, opt.test_model_path)
    model.load_state_dict(torch.load(opt.test_model_path))
    model.to(torch.device("cuda"))

    identity_list = get_lfw_list(opt.test_img_list)
    img_paths = [os.path.join(opt.test_img_root, each) for each in identity_list]

    model.eval()
    lfw_test(model, img_paths, identity_list, opt.test_img_list, opt.test_batch_size)




