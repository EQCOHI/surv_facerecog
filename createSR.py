import os

from misc_utils import retrieve_img_files
from test_script import generate_model_tester

target_set = 'QMUL'
# target_set = 'LFW'
# target_set = 'Helen'
# target_set = 'CelebA'

if __name__ == '__main__':

    lr_path = 'data/QMUL_origin/{}/'.format(target_set)
    # lr_path = '/home/choieq/dabu/arcface-pytorch/data/Datasets/QMUL/QMUL/Face_Verification_Test_Set/verification_images/'
    target_path = 'result/{}/'.format(target_set)
    os.system('rm -rf ' + target_path)
    os.makedirs(target_path, exist_ok=True)

    lr_list = retrieve_img_files(lr_path)
    model_type_list = ['DIC']  # , 'DIC', 'DICGAN', 'PFSR', 'FAN']

    # lr_path = '/home/choieq/dabu/arcface-pytorch/data/Datasets/lfw/LR/'
    # target_path = '/home/choieq/dabu/arcface-pytorch/data/Datasets/lfw/SR'

    # lr_list = os.listdir(lr_path)
    # model_type_list = ['DIC'] #, 'DIC', 'DICGAN', 'PFSR', 'FAN']


    for model_type in model_type_list:
        print('*****************************************************')
        model_tester = generate_model_tester(model_type, 'LFW')
        for lr_person in lr_list:
            print(lr_person)
            # # model_tester.make_SR_data(os.path.join(lr_path,lr_person), os.path.join(target_path, lr_person))
            # if not os.path.isdir(os.path.join(target_path, lr_person)):
            #     os.mkdir(os.path.join(target_path, lr_person))
            # lr_person_list = os.listdir(os.path.join(lr_path, lr_person))
            # for lr in lr_person_list:
            #     print(lr)

            # make sure the target dir exists
            dst_img_path = lr_person.replace(lr_path, target_path)
            dst_img_dir = dst_img_path[:dst_img_path.rfind('/') + 1]
            os.makedirs(dst_img_dir, exist_ok=True)

            model_tester.make_SR_data(lr_person, dst_img_dir)
