import argparse

from test_script import generate_model_tester

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--num-visualize', type=int, required=False, default=10, help='Number of Visual Comparison')

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()

    test_data_list = ['Helen']
    model_type_list = ['PFSR'] #['DICGAN', 'DIC']# , 'PFSR']

    for model_type in model_type_list:
        for test_data in test_data_list:
            # Even if same model, different pre_trained model according to test_data(celebA, Helen)
            model_tester = generate_model_tester(model_type, test_data)
            print('[*] Start test in {} using {}'.format(test_data, model_type))
            model_tester.test(test_data_type=test_data, n_visualize=args.num_visualize)
            print('\n')