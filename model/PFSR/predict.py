import torch

from model_tester import ModelTesterBase
from model.PFSR.model import Generator

from torchvision import transforms
from PIL import Image

class PFSR_ModelTester(ModelTesterBase):
    def __init__(self, data_type):
        self.model_name = 'PFSR'
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.generator = Generator().to(device)
        if "Align" in data_type:
            checkpoint_path = 'train-jobs/PFSR/generator_checkpoint_singleGPU.ckpt'#'train-jobs/PFSR/generator_checkpoint_singleGPU.ckpt'
        else:
            checkpoint_path = 'train-jobs/PFSR/unalign_trained_generator_checkpoint.ckpt'
        # generator_checkpoint_singleGPU
        # unalign_trained_generator_checkpoint
        self.g_checkpoint = torch.load(checkpoint_path, map_location='cuda:0')

        self.generator.load_state_dict(self.g_checkpoint['model_state_dict'], strict=False)
        self.step = self.g_checkpoint['step']
        self.alpha = self.g_checkpoint['alpha']
        iteration = self.g_checkpoint['iteration']
        print('pre-trained model is loaded step:%d, alpha:%d, iteration:%d'%(self.step, self.alpha, iteration))
        print(self.generator)

        self._16x16_down_sampling = transforms.Resize((16,16))
        self._64x64_down_sampling = transforms.Resize((64, 64))
        self._32x32_down_sampling = transforms.Resize((32, 32))

        self.transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ])

    def set_manual_seed(self):
        self.set_manual_seed_pt()

    def prepare_input(self, img_np):
        img_np = Image.fromarray(img_np)
        # img_np = transforms.CenterCrop((178,178))(img_np)
        # img_np = self._16x16_down_sampling(self._32x32_down_sampling(self._64x64_down_sampling(img_np)))

        img_tensor = self.transform(img_np)
        return img_tensor.float().cuda().unsqueeze(0)
        # return self.prepare_input_pt(img_np)

    def do_inference(self, img_tensor):

        with torch.no_grad():
            self.generator.eval()
            output = self.generator(img_tensor, self.step, self.alpha)
            output = 0.5*output + 0.5

        return self.convert_tensor_to_np_pt(output)