import torch

from model_tester import ModelTesterBase
from model.FAN.model.model import Generator
import torch.nn.functional as F

class FAN_ModelTester(ModelTesterBase):
    def __init__(self):
        self.model_name = 'FAN'

        # 4 is upscale factor
        self.model = Generator(4).eval().cuda()
        self.model.load_state_dict(torch.load('train-jobs/FAN/FAN_PSNR_X4.pth'))

    def set_manual_seed(self):
        self.set_manual_seed_pt()

    def prepare_input(self, img_np):
        img_tensor = self.prepare_input_pt(img_np)
        # img_tensor = F.interpolate(img_tensor, size=(32,32), mode='bicubic', align_corners=False)
        return img_tensor #self.prepare_input_pt(img_np)

    def do_inference(self, img_tensor):
        with torch.no_grad():
            out = self.model(img_tensor)
        return self.convert_tensor_to_np_pt(out)
