import torch

from model_tester import ModelTesterBase
import model.DIC.code.options.options as option
from model.DIC.code.solvers import create_solver
from model.DIC.code.networks import create_model

##
from collections import OrderedDict
from torchvision.transforms import Normalize
from model.DIC.code.utils import util
from torchvision import transforms
from skimage.transform import resize


class DIC_ModelTester(ModelTesterBase):
    def __init__(self, GAN, test_data):

        # For load pre-trained weight when
        if 'CelebA' in test_data:
            test_data = 'CelebA'
            if GAN == False:
                self.model_name = 'DIC'
                path_to_json = 'model/DIC/code/options/test/test_DIC_{}.json'.format(test_data)
                self.opt = option.parse(path_to_json)
                self.opt = option.dict_to_nonedict(self.opt)

            elif GAN == True:
                self.model_name = 'DICGAN'
                path_to_json = 'model/DIC/code/options/test/test_DICGAN_{}.json'.format(test_data)
                self.opt = option.parse(path_to_json)
                self.opt = option.dict_to_nonedict(self.opt)
        elif 'Helen' in test_data:
            test_data = 'Helen'
            if GAN == False:
                self.model_name = 'DIC'
                path_to_json = 'model/DIC/code/options/test/test_DIC_{}.json'.format(test_data)
                self.opt = option.parse(path_to_json)
                self.opt = option.dict_to_nonedict(self.opt)

            elif GAN == True:
                self.model_name = 'DICGAN'
                path_to_json = 'model/DIC/code/options/test/test_DICGAN_{}.json'.format(test_data)
                self.opt = option.parse(path_to_json)
                self.opt = option.dict_to_nonedict(self.opt)
        else:
            test_data = 'CelebA'
            if GAN == False:
                self.model_name = 'DIC'
                path_to_json = 'model/DIC/code/options/test/test_DIC_{}.json'.format(test_data)
                self.opt = option.parse(path_to_json)
                self.opt = option.dict_to_nonedict(self.opt)

            elif GAN == True:
                self.model_name = 'DICGAN'
                path_to_json = 'model/DIC/code/options/test/test_DICGAN_{}.json'.format(test_data)
                self.opt = option.parse(path_to_json)
                self.opt = option.dict_to_nonedict(self.opt)



        # create solver (and load model)
        self.solver = create_solver(self.opt)
        # self.model = create_model(self.opt)

        self.transform = transforms.Normalize((0.509 * self.opt['rgb_range'],
                                          0.424 * self.opt['rgb_range'],
                                          0.378 * self.opt['rgb_range']),
                                         (1.0, 1.0, 1.0))

    def set_manual_seed(self):
        self.set_manual_seed_pt()

    def prepare_input(self, img_np):
        # img_np = resize(img_np, (16,16), anti_aliasing=True, preserve_range=True)
        return self.prepare_input_pt(img_np)

    def do_inference(self, img_tensor):
        sr_list = []

        with torch.no_grad():
            """
            forward_func = self.model.forward
            sr_list, _ = forward_func(img_tensor)
            """
            # normalization same to official code
            img_tensor = torch.squeeze(img_tensor)
            img_tensor = self.transform(img_tensor)
            img_tensor = img_tensor.unsqueeze(0)

            self.solver.feed_data(img_tensor, need_HR=False, need_landmark=False)
            self.solver.test()
            visuals = self.solver.get_current_visual()
            sr_list.append(visuals['SR'][-1])


        return self.convert_tensor_to_np_pt(visuals['SR'][-1])
