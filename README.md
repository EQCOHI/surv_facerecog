# LR FaceRecognition

### Dataset Directory
* Download here
   https://drive.google.com/file/d/1XoZ0N5sJ5o22ZjXfZ7p0tCGce7mWCevM/view?usp=sharing
* FSR dataset 
```
data/
    |___HR/
        |___CelebA
                |__201418.png
                |__ ... 
        |___Helen
    |___LR/
        |___CelebA
                |__201418.png
                |__ ... 
        |___Helen
        |___QMUL
                |__4_cam1_1.jpg
```
* Recognition dataset 
```
recognition/
        |___data/
            |___Datasets/
                    |___lfw/
                        |___lfw_align/
                                    |___Aaron_Eckhart
                                        |___*.jpg
                                        |___ ...              
                        |___lfw_test_pair.txt
                    |___QMUL/
                        |___verification_images/
                                    |___4_cam1_1.jpg
                                    |___ ...
                        |___QMUL_test_pair.txt
```
### Pretrained Model Weight
* Face Super Resolution

    * Download here https://drive.google.com/file/d/11VpgeORajnXhglB1eXl8H1GmnQ49tBZF/view?usp=sharing
```
train-jobs/
    |__DIC
        |__*.pth
    |__FAN
        |__*.pth
    |__PFSR 
        |__*.pth
```
* Face Recognition
    
    * Download here https://drive.google.com/file/d/11zJqP4m6OX2hjUQ9hf_nt8a_2wmx8VJN/view?usp=sharing
```
recognition/
    |__checkpoints
        |__resnet18_110.pth
```

### Run 
* Face Super Resolution
1. For save Face Super Resolution image
    - python createSR.py
    - make sure LR image path & model_type_list = ['DIC',...]
    - result is saved ./result/
    
2. For measure PSNR/
    - python batch_test.py
    - test_data_list = ['Helen,...] & model_type_list = ['DIC',...]
    - result is saved ./script-output/ 
____________________________________________________
* Face Recognition
    - python ./recognition/test.py 
    - In ./recognition/checkpoints/config/config.py, make sure **test_img_root** your path
    
### LFW EER & ROC curve visualization
EER:0.007, AUC: 99.8, verification accuracy: 99.3
<div align="center">
<img src="recognition/visualize/lfw_eer.png" height="200px" alt="EER">
<img src="recognition/visualize/lfw_roc.png" height="200px" alt="ROC">
</div>