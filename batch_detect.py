import sys
import dlib
import cv2
import openface.align_dlib as openface
predictor_model = "shape_predictor_68_face_landmarks.dat"
import os

# Create a HOG face detector using the built-in dlib class
face_detector = dlib.get_frontal_face_detector()
face_pose_predictor = dlib.shape_predictor(predictor_model)
face_aligner = openface.AlignDlib(predictor_model)

source_path = '/home/choieq/dabu/arcface-pytorch/data/Datasets/lfw/Helen/helen_test/'
target_path = '/home/choieq/dabu/arcface-pytorch/data/Datasets/lfw/Helen/helen_test_hr'

person = os.listdir(source_path)
for item in person:
    os.mkdir(os.path.join(target_path, item))
    img_list = os.listdir(os.path.join(source_path,item))
    for img in img_list:
        # print('filename: ', os.path.join(source_path,item,img))

        filename=os.path.join(source_path,item,img)

        # Load the image
        image = cv2.imread(filename)

        # Run the HOG face detector on the image data
        detected_faces = face_detector(image, 1)

        # print("Found {} faces in the image file {}".format(len(detected_faces), filename))

        # EQ: if detected 2 boxes remove small one
        if len(detected_faces) >= 2:
            print("^^*")
            maxWidth = detected_faces[0].right() - detected_faces[0].left()
            maxidx = 0
            for i, face_rect in enumerate(detected_faces):
                w = face_rect.right() - face_rect.left()
                if w > maxWidth:
                    maxidx = i

            detected_faces = detected_faces[maxidx]

            try:
                faceimg = image[int(detected_faces.top()):int(detected_faces.bottom()), int(detected_faces.left()):int(detected_faces.right())]
                # EQ for remove noise
                # faceimg = cv2.medianBlur(faceimg, 7)
                # faceimg = cv2.resize(faceimg, dsize=(128,128), interpolation=cv2.INTER_CUBIC)
            except Exception as E:
                faceimg = image

                print(E)

            save_target = os.path.join(target_path, item, img)
            print("[*]Save! ", save_target)
            cv2.imwrite('{}.jpg'.format(save_target), faceimg)

        else:
            # Loop through each face we found in the image
            for i, face_rect in enumerate(detected_faces):
                # Detected faces are returned as an object with the coordinates
                # of the top, left, right and bottom edges
                print(
                    "- Face #{} found at Left: {} Top: {} Right: {} Bottom: {}".format(i, face_rect.left(), face_rect.top(),
                                                                                       face_rect.right(),
                                                                                       face_rect.bottom()))

                # EQ: Get the face's region for getting face image before alignment
                try:
                    faceimg = image[int(face_rect.top()-40):int(face_rect.bottom()+40), int(face_rect.left()-40):int(face_rect.right()+40)]
                    # EQ for remove noise
                    # faceimg = cv2.medianBlur(faceimg, 5)
                    # faceimg = cv2.resize(faceimg, dsize=(128,128), interpolation=cv2.INTER_LINEAR)
                except Exception as E:
                    faceimg = image

                    print(E)

                save_target = os.path.join(target_path, item, img)
                print("[*]Save! ", save_target)
                cv2.imwrite('{}.jpg'.format(save_target), faceimg)


            # for alignment
            """
            pose_landmarks = face_pose_predictor(image, face_rect)

            # Use openface to calculate and perform the face alignment
            alignedFace = face_aligner.align(128, image, face_rect,
                                             landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

            # Save the aligned image to a file
            save_target = os.path.join(target_path,item,img)
            print(save_target)
            cv2.imwrite('{}.jpg'.format(save_target), alignedFace)
            """