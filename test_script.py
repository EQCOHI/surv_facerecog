import argparse
import sys

sys.path.append('./code')

from utils import *  # ./code/utils.py
from model.DIC.predict import DIC_ModelTester
from model.PFSR.predict import PFSR_ModelTester
from model.FAN.predict import FAN_ModelTester

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, required=True,
                        help='select model for Face SR 1. DIC, 2. PFSR')
    parser.add_argument('--testData', type=str, required=True, hlep='select test img dataset')
    parser.add_argument('--num-visualize', type=int, required=False, default=3, help='Number of Visual Comparision')

    args = parser.parse_args()
    return args

def generate_model_tester(model_type, test_data):
    if model_type == 'DIC':
        return DIC_ModelTester(GAN=False, test_data=test_data)
    elif model_type == 'DICGAN':
        return DIC_ModelTester(GAN=True, test_data=test_data)
    elif model_type == 'PFSR':
        return PFSR_ModelTester()
    elif model_type == 'FAN':
        return FAN_ModelTester()
    #
    # elif model_type == 'PFSR':
    #     return SAN_ModelTester(scale)
