from glob import glob


def retrieve_img_files(root_dir):
    file_list = glob(root_dir + '**/*.*', recursive=True)
    file_list.sort()
    file_list = [fn for fn in file_list if fn[-4:] in ['.jpg', '.png', '.JPG', '.PNG']]
    return file_list
