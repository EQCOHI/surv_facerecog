import os
import cv2
import sys
from tqdm import tqdm
sys.path.append('code/')
from utils import get_psnr, get_ssim

sr_path = 'result/'
ref_path = '/home/choieq/dabu/arcface-pytorch/data/Datasets/lfw/lfw-align-128/'


sr_list = [
    'bilinear', 'bilinear2', 'DIC', 'DICGAN', 'FAN_case1', 'FAN_case2', 'FAN_case3', 'PFSR'
]

for item in tqdm(sr_list):
    psnr = []
    ssim = []
    if item == 'FAN_case3':
        sr_dir = os.path.join(sr_path + item + '/LFW_32/')
        sr_list = os.listdir(sr_dir)
    else:
        sr_dir = os.path.join(sr_path+item+'/LFW/')
        sr_list = os.listdir(sr_dir)

    for id in sr_list:
        # print(id)
        img_list = os.listdir(os.path.join(sr_dir,id))

        for img in img_list:
            loaded_img = cv2.imread(os.path.join(sr_dir,id,img))
            loaded_img = cv2.cvtColor(loaded_img, cv2.COLOR_BGR2RGB)
            ref_img = cv2.imread(os.path.join(ref_path, id, img))
            ref_img = cv2.cvtColor(ref_img, cv2.COLOR_BGR2RGB)
            psnr.append(get_psnr(loaded_img, ref_img, scale=8))
            ssim.append(get_ssim(loaded_img, ref_img))

    print("{} avg psnr".format(item), sum(psnr)/len(psnr))
    print("{} avg ssim".format(item), sum(ssim) / len(ssim))