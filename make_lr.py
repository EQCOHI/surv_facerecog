import cv2
import numpy as np
import os
from skimage.transform import resize
from torchvision.transforms import Resize
from torchvision import transforms
from PIL import Image,ImageFilter
from misc_utils import retrieve_img_files

source_path = '/home/choieq/dabu/surv_facerecog/data/HR/OurCelebA'

# target_set = 'Helen'
# target_set = 'CelebA'
target_set = 'PFSR_OurCelebA'
dst_path = 'data/LR/' + target_set + '/'
os.system('rm -rf ' + dst_path)

hr_files = retrieve_img_files(source_path)

_16x16_down_sampling = transforms.Resize((16, 16))
_64x64_down_sampling = transforms.Resize((64, 64))
_32x32_down_sampling = transforms.Resize((32, 32))

def degradation(hr_image):
    sigma, noise_level, jpeg_level = 2.0, 5.0, 100.

    kernel_1d = cv2.getGaussianKernel(21, sigma)
    kernel_2d = np.outer(kernel_1d, kernel_1d.transpose())
    blur_hr_image = cv2.filter2D(np.array(hr_image), -1, kernel_2d)
    blur_hr_image = Image.fromarray(blur_hr_image)

    # lr_image = np.asarray(hr_image.resize((16, 16), resample=Image.BICUBIC))
    lr_image = np.asarray(blur_hr_image.resize((16, 16), resample=Image.BICUBIC))
    noise = np.random.rand(lr_image.shape[0], lr_image.shape[1], lr_image.shape[2]) * noise_level
    lr_image = lr_image + noise
    lr_image = (np.clip(lr_image.round(), 0, 255)).astype('uint8')

    # padding = 12
    # lr_image = cv2.copyMakeBorder(lr_image, padding, padding, padding, padding, cv2.BORDER_REPLICATE)
    _, encimg = cv2.imencode(".jpg", lr_image, (cv2.IMWRITE_JPEG_QUALITY, jpeg_level))
    lr_image = cv2.imdecode(encimg, 1)
    # center crop to 16x16
    # w, h = 16, 16
    # lr_image = lr_image[padding:padding + h, padding:padding + w]
    return lr_image

for i, item in enumerate(hr_files):
    print(i, ":", item)

    # check if the dst directory exist
    dst_img_path = item.replace(source_path, dst_path)
    dst_img_path = dst_img_path.replace('jpg', 'png')
    dst_img_dir = dst_img_path[:dst_img_path.rfind('/') + 1]
    os.makedirs(dst_img_dir, exist_ok=True)

    # PFSR version ################################################################################

    hr_img = cv2.imread(item)
    hr_img = cv2.cvtColor(hr_img, cv2.COLOR_BGR2RGB)
    hr_img = Image.fromarray(hr_img)

    # hr_img = transforms.CenterCrop((178,178))(hr_img)
    # hr_img = transforms.Resize((128,128))(hr_img)

    lr_img = _16x16_down_sampling(_32x32_down_sampling(_64x64_down_sampling(hr_img)))
    lr_img.save(dst_img_path, format="png")

    # DIC version ################################################################################
    # hr_img = Image.open(item)
    # hr_img = hr_img.resize((128, 128), resample=Image.BICUBIC)
    # lr_img = hr_img.resize((16, 16), resample=Image.BICUBIC)
    # lr_img.save(dst_img_path, format="png")